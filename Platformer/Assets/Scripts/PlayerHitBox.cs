﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerHitBox : MonoBehaviour
{
    public TestObj tobj;
    public Boss bossobj;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "testobj")
        {
            tobj.Gothit();

        }
        else if (collision.gameObject.tag == "boss")
        {

            bossobj.boss_hit();
        }
    }
}