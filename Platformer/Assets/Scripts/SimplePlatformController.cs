﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SimplePlatformController : MonoBehaviour {

	[HideInInspector] public bool facingRight = true;
	 public bool jump = false;
	public float moveForce = 365f;
	public float maxSpeed = 5f;
	public float jumpForce = 1000f;
	public Transform groundCheck;
    public int PlayerHP = 100;


	private bool grounded = false;
	private Animator anim;
	private Rigidbody2D rb2d;
    private Animator animator;
    public AudioClip hitSound;
    //public TestObj tobj;
    public GameObject SpawnObject;
    public Transform HBSpawnPoint;
    public bool HitBoxState = false;
    public int speed = 10;
	public Text OpenText;
	public Image OpenImage;
	public Text EndText;
	public AudioSource BGM;
	public AudioSource SoundM;
	public int hideOpen = 1;
    //public Boss bossobj;


	// Use this for initialization
	void Awake () 
	{	
        
		anim = GetComponent<Animator>();
		rb2d = GetComponent<Rigidbody2D>();
        SpawnObject.SetActive(HitBoxState);
		EndText.enabled = false;
		Invoke ("HideOpening", 3);
		print ("awake");


	}

	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKeyDown ("escape")) {
			Application.Quit ();
		}
        //Vector3 direction = Camera.main.transform.forward;
        //direction.y = 0;

        //transform.position += direction * speed * Time.deltaTime;
        if (PlayerHP <= 0){
            player_die();
        }
		grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));


		if (Input.GetButtonDown("Jump") && grounded)
		{
			jump = true;
		}
        if (Input.GetKeyDown("x")){
            
            SpawnHitBox();
            anim.SetTrigger("PlayerHit");
            //if (anim.GetCurrentAnimatorStateInfo(0).IsName("PlayerHit"))
            //{
            //    
            //}
            SoundManager.instance.PlaySingle(hitSound);
            Invoke("kill_box", 1);
            //SpawnObject.SetActive(HitBoxState);

        }

	}

	void FixedUpdate()
	{
		float h = Input.GetAxis("Horizontal");

		anim.SetFloat("Speed", Mathf.Abs(h));

		if (h * rb2d.velocity.x < maxSpeed)
			rb2d.AddForce(Vector2.right * h * moveForce);

		if (Mathf.Abs (rb2d.velocity.x) > maxSpeed)
			rb2d.velocity = new Vector2(Mathf.Sign (rb2d.velocity.x) * maxSpeed, rb2d.velocity.y);

		if (h > 0 && !facingRight)
			Flip ();
		else if (h < 0 && facingRight)
			Flip ();

		if (jump)
		{
			rb2d.AddForce(new Vector2(0f, jumpForce));
			jump = false;
		}
	}
    public int GetHP(){
        return PlayerHP;
    }


	void Flip()
	{
		facingRight = !facingRight;
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

    //private void OnTriggerEnter2D(Collider2D collision)
    //{
        //if (collision.gameObject.tag == "testobj"){
        //    tobj.Gothit();
            
        //}
        //else if (collision.gameObject.tag == "boss"){
            
        //    bossobj.boss_hit();
        //}
    //}
    void SpawnHitBox(){
        //Instantiate(SpawnObject, HBSpawnPoint.position, Quaternion.identity);
        HitBoxState = true;

        SpawnObject.SetActive(HitBoxState);
        
    }
    void ChangeState(){
        if (HitBoxState == true){
            HitBoxState = false;
        } else {
            HitBoxState = true;
        }
        
    }
    void kill_box(){
        SpawnObject.SetActive(false);
    }
    public void player_get_hit(){
        PlayerHP -= 10;
        anim.SetTrigger("PlayerDamaged");
        
    }
    void player_die(){
		Time.timeScale = 0;
		BGM.Stop();
		SoundM.Stop ();
		OpenImage.enabled = true;
		EndText.enabled = true;
		if (Input.GetKeyDown ("z")) {
			Time.timeScale = 1;
			Scene scene = SceneManager.GetActiveScene(); SceneManager.LoadScene(scene.name);
			print ("reload");


		}

			

    }
	void HideOpening(){
		OpenImage.enabled = false;
		OpenText.enabled = false;

	}
}