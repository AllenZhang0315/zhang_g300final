﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour {
    public Slider health;
    public SimplePlatformController player;

	// Use this for initialization
	void Start () {
        health = GetComponent<Slider>();
		
	}
	
	// Update is called once per frame
	void Update () {
        health.value = player.GetHP();
        
	}
}
