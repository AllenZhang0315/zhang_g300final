﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Invoke("kill_me",1);
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "player")
        {
            collision.GetComponent<SimplePlatformController>().player_get_hit();



        }
    }
    void kill_me(){
        Destroy(gameObject);
    }
}
