﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Boss : MonoBehaviour
{
    public GameObject BossObj;
    public int BossHP = 100;
    public Transform FirePoint1;
    public Transform RollingfirePoint;
    public GameObject FireBall;
    public GameObject ExplosionBall1;
    public GameObject ExplosionBall2;
    private Rigidbody2D rb2d;
    public Rigidbody2D rigid;
    private int fireball_timer = 1600;

    private Animator animator;
    public Vector2[] VectorLst;
    public Transform[] DropFireLst;
    public AudioClip[] AttackSound;
    public int BossState = 1;
	public Image OpenImage;
	public Text EndText;
	public AudioSource BGM;
	public AudioSource SoundM;


    // Use this for initialization
    void Awake()
    {
		EndText.enabled = false;
        animator = GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {   
        if (BossHP <= 0)
		{
			boss_die ();


        }

        fireball_timer -= 1;
        if (fireball_timer <= 0)
        {
            if (BossHP >= 90){
                
                SpawnFireBall3();
                Invoke("SpawnFireBall3",1);
                Invoke("SpawnFireBall3",1);
                
            } else if (BossHP < 90 & BossHP >= 45){
                if (BossState == 1)
                {
                    animator.SetTrigger("BossChange");
                    BossState = 2;
                }

                
                int skillIndex_choice = Random.Range(0, 5);

                if (skillIndex_choice > 3)
                {
                    spawn_dropping_fire();

                }
                else
                {
                   
                    SpawnFireBall3();
                    Invoke("SpawnFireBall3", 1);
                    Invoke("SpawnFireBall3", 1);
                    Invoke("SpawnFireBall3", 1);
                    Invoke("SpawnFireBall3", 1);
                    Invoke("SpawnFireBall3", 1);

                }
                
                
            } else {
                if (BossState == 2)
                {
                    animator.SetTrigger("BossChange");
                    BossState = 3;
                }
                int skillIndex_choice = Random.Range(0, 5);

                if (skillIndex_choice == 1 | skillIndex_choice == 2)
                {
                    spawn_dropping_fire();

                }
                else if (skillIndex_choice == 0)
                {
                    spawn_rolling_fire();


                }
                else
                {
                    SpawnFireBall3();
                    Invoke("SpawnFireBall3", 1);
                    Invoke("SpawnFireBall3", 1);
                    Invoke("SpawnFireBall3", 1);
                    Invoke("SpawnFireBall3", 1);
                    Invoke("SpawnFireBall3", 1);

                }
            }





            fireball_timer = 2000;
        }





    }




    public void boss_hit()
    {
        BossHP -= 1;
        
    }
    public int GetBossHP(){
        return BossHP;
    }
    public void SpawnFireBall3()
    {
        int index = Random.Range(0, VectorLst.Length);
        Vector2 firePosition = VectorLst[index];
        animator.SetTrigger("BossAttack");

        rb2d = FireBall.GetComponent<Rigidbody2D>();

        rigid = Instantiate(rb2d, FirePoint1.position, Quaternion.identity);
        rigid.AddForce(firePosition * 500);

        //Instantiate(FireBall, FirePoint1.position, Quaternion.identity);



        //Invoke("kill_fireball", 1);
        //Invoke("kill_fireball(bulletInstance)", 1);


        //Instantiate(FireBall, FirePoint3.position, Quaternion.identity);
        //Instantiate(FireBall, FirePoint5.position, Quaternion.identity);
        //rb2d.AddForce(Vector2.right * 1000*-1);


    }
    public void spawn_rolling_fire(){
        animator.SetTrigger("BossAttack");
        SoundManager.instance.RandomizeSfx(AttackSound);
        rb2d = ExplosionBall1.GetComponent<Rigidbody2D>();
        rigid = Instantiate(rb2d, RollingfirePoint.position, Quaternion.Euler(new Vector3(0, 0, 1))) as Rigidbody2D;

        rigid.AddForce(new Vector2(1,0) * 300);
        
    }
    public void spawn_dropping_fire(){
        int index = Random.Range(0, DropFireLst.Length);
        Transform firePosition = DropFireLst[index];
        animator.SetTrigger("BossAttack");
        SoundManager.instance.RandomizeSfx(AttackSound);
        rb2d = ExplosionBall2.GetComponent<Rigidbody2D>();
        rigid = Instantiate(rb2d, firePosition.position, Quaternion.identity);

        rigid.AddForce(new Vector2(0, -1) * 800);
    }
    public void kill_fireball(){
        Destroy(rigid);
    }
	void boss_die(){
		Time.timeScale = 0;
		BGM.Stop();
		SoundM.Stop ();
		OpenImage.enabled = true;
		EndText.enabled = true;
		if (Input.GetKeyDown ("z")) {
			Time.timeScale = 1;
			Scene scene = SceneManager.GetActiveScene();
			SceneManager.LoadScene(scene.name);

		}
}
}