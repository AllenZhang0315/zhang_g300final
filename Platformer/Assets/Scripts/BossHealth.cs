﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossHealth : MonoBehaviour {
    public Slider health;
    public Boss boss;

	// Use this for initialization
	void Start () {
        health = GetComponent<Slider>();
        
		
	}
	
	// Update is called once per frame
	void Update () {
        health.value = boss.GetBossHP();
		
	}
}
